using UnityEngine;

public class SnowballController : MonoBehaviour
{
    [SerializeField] private Rigidbody snowBallPrefab;
    [SerializeField] private Transform hand;
    [SerializeField] private float speed = 1f;

    [SerializeField] private float step = 1f;
    private float nextShoot;
    
    private void Update()
    {
        if (Time.time > nextShoot)
        {
            Shoot();
            nextShoot = Time.time + step;
        }
    }
    private void Shoot()
    {
        var snowball = Instantiate(snowBallPrefab,
            hand.position, hand.rotation);
        snowball.AddForce(snowball.transform.up
            * speed, ForceMode.Impulse);
    }
}
